#!/usr/bin/python
import sys

code =  '#!/bin/bash -l\n';
code += '#\n';
code += '#SBATCH -D ' + str(sys.argv[13]) + '/workspace/lbm/\n';
code += '#SBATCH -o ' + str(sys.argv[13]) + '/workspace/lbm/results/' + str(sys.argv[12]) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '.o.txt\n'
code += '#SBATCH -e ' + str(sys.argv[13]) + '/workspace/lbm/results/' + str(sys.argv[12]) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '.e.txt\n'
code += '#SBATCH -J lbm_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '\n';
code += '#SBATCH --get-user-env\n';
code += '#\n';
code += '#SBATCH --constraint=gpu\n';
code += '#SBATCH --partition=normal\n';
# code += '#Total number of nodes:\n';
# code += '#SBATCH --nodes=' + str(int(sys.argv[7])) + '\n';
code += "#Total number of tasks/ranks/processes:\n";
code += "#SBATCH --ntasks=" + str(int(sys.argv[12])) + "\n";
code += '#Number of tasks/ranks/processes per node:\n';
code += '#SBATCH --ntasks-per-node=' + str(sys.argv[8]) + '\n';
code += '#Number of threads per task/rank/process:\n';
code += '#SBATCH --cpus-per-task=' + str(int(sys.argv[11])) + '\n';
code += '#Deactivate hyperthreading\n';
code += '#SBATCH --hint=nomultithread\n';
code += '#SBATCH --time=00:10:00\n';
code += '#\n';
code += '#SBATCH --mail-type=end\n';
code += '#SBATCH --mail-user=riesinge@in.tum.de\n';
code += '\n';
code += 'module load daint-gpu\n';
code += 'module unload PrgEnv-cray\n';
code += 'module load PrgEnv-intel\n';
# code += 'module load intel/17.0.1.132\n';
code += 'module load craype-accel-nvidia60\n';
code += 'module load cray-netcdf-hdf5parallel/4.4.1.1\n';
code += '\n';
code += 'export OMP_NUM_THREADS=' + str(sys.argv[11]) + '\n';
code += 'export KMP_AFFINITY=granularity=fine,compact,1,0\n';
code += '\n';
# GPU-only
code += 'srun -n ' + str(int(sys.argv[12])) + ' bin/lbm configurations/daint_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_0.0.xml\n'
# CPU-only
code += 'srun -n ' + str(int(sys.argv[12])) + ' bin/lbm configurations/daint_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_1.0.xml\n'
for ratio in range(15, 0, -1):
	code += 'srun -n ' + str(int(sys.argv[12])) + ' bin/lbm configurations/daint_' + str(int(sys.argv[12])) + '_' + str(sys.argv[1]) + 'x' + str(sys.argv[2]) + 'x' + str(sys.argv[3]) + '_' + str(sys.argv[4]) + 'x' + str(sys.argv[5]) + 'x' + str(sys.argv[6]) + '_' + str(float(ratio) / float(100)) + '.xml\n'
# code += 'srun -n ' + str(sys.argv[12]) + ' ' + str(sys.argv[13]) + '/workspace/testcpp/bin/hybrid_affinity\n'
	
jobscript = open('jobscript.sh', 'w')
jobscript.write(code)
jobscript.close()

