Features
========
This C++ code implements the lattice Boltzmann method with the following features:
- Supports execution on CPU and GPU, also simultaneously (heterogeneous computing)
- CPU kernels parallelized with OpenMP, GPU kernels implemented with CUDA
- Parallelized with MPI (hybrid implementation)
- Communication hided with computation
- Measured scalability up to 2048 GPUs and 24576 CPU cores
- Export of simulation data as netCDF or VTK

Installation
============
lbm is build via make. For every target system, an individual makefile with system specific parameters has to be provided by the user. A default execution of make without such a system-specific makefile is not possible. In addition, three additional options can be passed to make influencing the resulting executable. There are two build targets: clean and all.

All system specific individual makefiles have to include common.make at the end of the file. They enable to set system-specific parameters like compilers, compute capabilities, compiler options, etc.. The following tables lists parameters for system-specific makefiles (if they are not already set by environment variables):

| Name                 | Description                                                                             |
| -------------------- | --------------------------------------------------------------------------------------- |
| `CUDA_HOME`          | Root folder of CUDA installation                                                        |
| `NETCDF_HOME`        | Root folder of (parallel) netCDF installation                                           |
| `CCLIBDIR`           | Directory paths for optional libraries required during link time of the C code files    |
| `CXXLIBDIR`          | Directory paths for optional libraries required during link time of the C++ code files  |
| `CUDALIBDIR`         | Directory paths for optional libraries required during link time of the CUDA code files |
| `CCINCLUDES`         | Directory paths for optional headers required during compilation of the C code files    |
| `CXXINCLUDES`        | Directory paths for optional headers required during compilation of the C++ code files  |
| `CUDAINCLUDES`       | Directory paths for optional headers required during compilation of the CUDA code files |
| `CCLIB`              | Actual libraries required during link time of the C code files                          |
| `CXXLIB`             | Actual libraries required during link time of the C++ code files                        |
| `CUDALIB`            | Actual libraries required during link time of the CUDA code files                       |
| `COMPUTE_CAPABILITY` | Compute capability of the utilized GPUs to compile architecture specific                |
| `CC`                 | (Path and) name of C compiler (e.g. gcc, icc)                                           |
| `CXX`                | (Path and) name of C++ compiler (e.g. g++, icpc)                                        |
| `LINKER`             | (Path and) name of linker                                                               |
| `NVCC`               | (Path and) name of CUDA compiler (e.g. nvcc)                                            |
| `NVCCLINKER`         | (Path and) name of linker for multiple CUDA object files (e.g. nvcc)                    |
| `CCFLAGS`            | Optional flags for C compiler                                                           |
| `CXXFLAGS`           | Optional flags for C++ compiler                                                         |
| `NVCCFLAGS`          | Optional flags for CUDA compiler                                                        |
| `LINKERFLAGS`        | Optional flags for `LINKER`                                                             |
| `NVCCLINKERFLAGS`    | Optional flags for `NVCCLINKER`                                                         |

There are six example files in the root directory of the repository: atsccs62.make (for a local workstation), daint.make (for Piz Daint cluster), hydra.make (for Hydra cluster), hydra-interactive.make (for interactive test system of Hydra cluster), mac-cluster.make (for MAC cluster's nvd partition), and tsubame.make (for TSUBAME2.5 cluster).

There are three options which can be passed to make:
- USE_MPI: If set to 1, a MPI capable parallel binary is generated. Having a non-MPI version can be useful, e.g. to profile GPU kernels. MPI is optional.
- PAR_NETCDF: If set to 1, parallel netCDF is enabled. This is especially useful if it comes to output of simulation data on many ranks at the same time. Not all systems support parallel but only serial netCDF. Parallel netCDF is optional.
- INSTRUMENT: To profile lbm, the code has to be instrumented during compile time. Two different instrument options are available which lead to the usage of the corresponding tools: scalasca and scorep. Instrumentation is optional.

A valid make call to compile lbm could look as follows:
`make -f mac-cluster.make -j $OMP_NUM_THREADS all USE_MPI=1 PAR_NETCDF=1 INSTRUMENT=scorep`

Running
=======
A valid XML configuration file specifying the physics, domain, and parallel setup of the simulation has to be passed when starting the application. It's not possible to pass these parameters via command line. `configurations/default.xml` is an example for such an XML configuration file. All listed parameters in `default.xml` have to be provided by the user. Additional parameters are ignored.

The following table provides a brief description of some parameters:

| Parameter               | Description                                                                                                 |
| ----------------------- | ----------------------------------------------------------------------------------------------------------- |
| `<velocity>`            | Velocity im m/s of the driving lid. Only *x* value is considered                                            |
| `<viscosity>`           | Kinematic viscosity. If a negative value is passed, an appropriate viscosity for Re = 1000 is set           |
| `<domain-size>`         | Domain size in lattice cells                                                                                |
| `<domian-length>`       | Domain length in m. Ratio of directions has to fit the ration of the domain size                            |
| `<subdomain-num>`       | Number of subdomains. Product has to match the number of launched MPI processes                             |
| `<cpu-subdomain-ratio>` | Share of the CPU-part of a subdomain to the total size of the subdomain. Ratio refers to the *y*-direction  |
| `<loops>`               | Number of timesteps to run. An even number has to be provided due to alpha- and beta-steps                  |
| `<timestep>`            | Timestep size in s. If CFL condition is not satisfied, timestep size is automatically adapted (see logging) |
| `<benchmark>`           | Option to output benchmark values such as memory bandwidth utilization and LUPS rate in specified folder    |
| `<logging>`             | Option to output per process log files in specified folder                                                  |
| `<validation>`          | Option to validate multi-process results with single-process results. Differences are stored in specified folder. Currently not implemented anymore, thus has no effect |
| `<visualization>`       | Option to store simulation data for visualization in specified folder. `<rate>` specifies the number of timesteps after which a snapshot is returned. netCDF or VTK output has to be hard coded |
| `<block-configuration>` | Blocking size of alpha- and beta-kernel on CPU to increase cache efficiency                                 |
| `<grid-configuration>`  | Parallel setup of alpha- and beta-kernel on GPU                                                             |

Running lbm could look as follows:
`./lbm configurations/default.xml`

Running lbm with MPI could look as follows:
`mpiexec -n 2 lbm configurations/default.xml`

Remarks
========
- lbm always requires a CUDA installation (but no GPUs) to be compiled even if CPU-only experiments should be carried out. The reason is the usage of the `cudaMemcpy3D()` function even for host memory to host memory copies. Hence, `cuda_runtime.h` is necessary during compile time and `libcudart` is required during link time.
- The only necessary library (besides CUDA) for lbm is (parallel) netCDF. It is also required if no or only VTK export is done.
- lbm brings code for an XML importer (`external/tinyxml2`) and a VTK exporter (`src/libvis/CLbmVisualizationVTK`).
- Even if the makefiles offer dedicated parameters for C code files and a dedicated C compiler, lbm does not contain any C code and thus, according values have no effect.
- There is no OpenCL code.

