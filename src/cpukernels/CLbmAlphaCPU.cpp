/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <omp.h>

#include "../libmath/CMath.hpp"
#include "CLbmAlphaCPU.hpp"

template<class T>
CLbmAlphaCPU<T>::CLbmAlphaCPU(
        CVector<3, int> domainSize,
        CVector<3, T> gravitation) :
        domainSize(domainSize),
        gravitation(gravitation),
        numOfCells(domainSize.elements())
{
}

template<class T>
CLbmAlphaCPU<T>::~CLbmAlphaCPU()
{
}

template<class T>
void CLbmAlphaCPU<T>::alphaKernelCPU(
        T* densityDistributions,
        Flag* flags,
        T* densities,
        T* velocities,
        const T tauInv,
        const T drivenCavityVelocity,
        CVector<3, int> origin,
        CVector<3, int> size,
        CVector<3, int> blockDim,
        const bool storeDensities,
        const bool storeVelocities)
{
    /*
     * Due to OpenMP restrictions (class members can't be explicitly set private
     * or shared), class members have to be explicitly copied to use them
     * properly in OpenMP parallel region/spawning of tasks.
     */
    CVector<3, int> domainSizeCpy = domainSize;
    CVector<3, T> gravitationCpy = gravitation;
    int numOfCellsCpy = numOfCells;
    
    int blockIdxLimZ = ((size.data[2] - 1) / blockDim.data[2]) + 1;
    for (int blockIdxZ = 0; blockIdxZ < blockIdxLimZ; blockIdxZ++)
    {
        int blockIdxLimY = ((size.data[1] - 1) / blockDim.data[1]) + 1;
        for (int blockIdxY = 0; blockIdxY < blockIdxLimY; blockIdxY++)
        {
            int blockIdxLimX = ((size.data[0] - 1) / blockDim.data[0]) + 1;
            for (int blockIdxX = 0; blockIdxX < blockIdxLimX; blockIdxX++)
            {
#pragma omp task default(none) firstprivate(domainSizeCpy, gravitationCpy, numOfCellsCpy, densityDistributions, flags, densities, velocities, tauInv, drivenCavityVelocity, origin, size, blockDim, storeDensities, storeVelocities, blockIdxX, blockIdxY, blockIdxZ)
                {
                    int elementIdxLimZ = CMath<int>::min(blockDim.data[2], size.data[2] - blockIdxZ * blockDim.data[2]);
                    for (int elementIdxZ = 0; elementIdxZ < elementIdxLimZ; elementIdxZ++)
                    {
                        int elementIdxLimY = CMath<int>::min(blockDim.data[1], size.data[1] - blockIdxY * blockDim.data[1]);
                        for (int elementIdxY = 0; elementIdxY < elementIdxLimY; elementIdxY++)
                        {
                            int elementIdxLimX = CMath<int>::min(blockDim.data[0], size.data[0] - blockIdxX * blockDim.data[0]);
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1600)
#pragma simd vecremainder
#endif
                            for (int elementIdxX = 0; elementIdxX < elementIdxLimX; elementIdxX++)
                            {
                                T dd0, dd1, dd2, dd3, dd4, dd5, dd6, dd7, dd8, dd9, dd10, dd11, dd12, dd13, dd14, dd15, dd16, dd17, dd18;
                                T rho;
                                T velX, velY, velZ;
                                T velSqr, velComb, velCombSqr;
                                T dd_param, tmp;
                                int X = blockIdxX * blockDim.data[0] + elementIdxX;
                                int Y = blockIdxY * blockDim.data[1] + elementIdxY;
                                int Z = blockIdxZ * blockDim.data[2] + elementIdxZ;
                                int id = (origin.data[2] + Z) * (domainSizeCpy.data[0] * domainSizeCpy.data[1]) + (origin.data[1] + Y) * domainSizeCpy.data[0] + (origin.data[0] + X);
                                T* current_dds = &densityDistributions[id];

                                if (flags[id] == GHOST_LAYER)
                                    continue;

                                /*
                                 * +++++++++++
                                 * +++ DD0 +++
                                 * +++++++++++
                                 *
                                 * 0-3: f(1,0,0), f(-1,0,0),  f(0,1,0), f(0,-1,0)
                                 */
                                dd0 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho = dd0;
                                velX = dd0;
                                dd1 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd1;
                                velX -= dd1;
                                dd2 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd2;
                                velY = dd2;
                                dd3 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd3;
                                velY -= dd3;

                                /*
                                 * +++++++++++
                                 * +++ DD1 +++
                                 * +++++++++++
                                 *
                                 * 4-7: f(1,1,0), f(-1,-1,0), f(1,-1,0), f(-1,1,0)
                                 */
                                dd4 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd4;
                                velX += dd4;
                                velY += dd4;
                                dd5 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd5;
                                velX -= dd5;
                                velY -= dd5;
                                dd6 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd6;
                                velX += dd6;
                                velY -= dd6;
                                dd7 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd7;
                                velX -= dd7;
                                velY += dd7;

                                /*
                                 * +++++++++++
                                 * +++ DD2 +++
                                 * +++++++++++
                                 *
                                 * 8-11: f(1,0,1), f(-1,0,-1), f(1,0,-1), f(-1,0,1)
                                 */
                                dd8 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd8;
                                velX += dd8;
                                velZ = dd8;
                                dd9 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd9;
                                velX -= dd9;
                                velZ -= dd9;
                                dd10 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd10;
                                velX += dd10;
                                velZ -= dd10;
                                dd11 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd11;
                                velX -= dd11;
                                velZ += dd11;

                                /*
                                 * +++++++++++
                                 * +++ DD3 +++
                                 * +++++++++++
                                 *
                                 * 12-15: f(0,1,1), f(0,-1,-1), f(0,1,-1), f(0,-1,1)
                                 */
                                dd12 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd12;
                                velY += dd12;
                                velZ += dd12;
                                dd13 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd13;
                                velY -= dd13;
                                velZ -= dd13;
                                dd14 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd14;
                                velY += dd14;
                                velZ -= dd14;
                                dd15 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd15;
                                velY -= dd15;
                                velZ += dd15;

                                /*
                                 * +++++++++++
                                 * +++ DD4 +++
                                 * +++++++++++
                                 *
                                 * 16-18: f(0,0,1), f(0,0,-1), f(0,0,0)
                                 */
                                dd16 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd16;
                                velZ += dd16;
                                dd17 = *current_dds;
                                current_dds += numOfCellsCpy;
                                rho += dd17;
                                velZ -= dd17;
                                dd18 = *current_dds;
                                rho += dd18;
                                
                                current_dds = &densityDistributions[id];
                                
                                switch(flags[id])
                                {
                                    case (FLUID):
                                        dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);

                                        /*
                                         * +++++++++++
                                         * +++ DD0 +++
                                         * +++++++++++
                                         */
                                        tmp = gravitationCpy.data[0] * ((T)1/(T)18) * rho;
                                        velSqr = velX * velX;
                                        dd1 += tauInv * (eq_dd_a1(velX, velSqr, dd_param) - dd1);
                                        dd1 -= tmp;
                                        *current_dds = dd1;
                                        current_dds += numOfCellsCpy;
                                        dd0 += tauInv * (eq_dd_a0(velX, velSqr, dd_param) - dd0);
                                        dd0 += tmp;
                                        *current_dds = dd0;
                                        current_dds += numOfCellsCpy;
                                        tmp = gravitationCpy.data[1] * ((T)-1/(T)18) * rho;
                                        velSqr = velY * velY;
                                        dd3 += tauInv * (eq_dd_a1(velY, velSqr, dd_param) - dd3);
                                        dd3 -= tmp;
                                        *current_dds = dd3;
                                        current_dds += numOfCellsCpy;
                                        dd2 += tauInv * (eq_dd_a0(velY, velSqr, dd_param) - dd2);
                                        dd2 += tmp;
                                        *current_dds = dd2;
                                        current_dds += numOfCellsCpy;
                                        
                                        /*
                                         * +++++++++++
                                         * +++ DD1 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velY;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] - gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd5 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd5);
                                        dd5 -= tmp;
                                        *current_dds = dd5;
                                        current_dds += numOfCellsCpy;
                                        dd4 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd4);
                                        dd4 += tmp;
                                        *current_dds = dd4;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velY;
                                        velCombSqr = velComb*velComb;
                                        tmp = (gravitationCpy.data[0] + gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd7 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd7);
                                        dd7 -= tmp;
                                        *current_dds = dd7;
                                        current_dds += numOfCellsCpy;
                                        dd6 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd6);
                                        dd6 += tmp;
                                        *current_dds = dd6;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD2 +++
                                         * +++++++++++
                                         */
                                        velComb = velX+velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] + gravitationCpy.data[2]) * ((T)1/(T)36) * rho;
                                        dd9 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd9);
                                        dd9 -= tmp;
                                        *current_dds = dd9;
                                        current_dds += numOfCellsCpy;
                                        dd8 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd8);
                                        dd8 += tmp;
                                        *current_dds = dd8;
                                        current_dds += numOfCellsCpy;
                                        tmp = (gravitationCpy.data[0] - gravitationCpy.data[2]) * ((T)1/(T)36) * rho;
                                        velComb = velX - velZ;
                                        velCombSqr = velComb * velComb;
                                        dd11 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd11);
                                        dd11 -= tmp;
                                        *current_dds = dd11;
                                        current_dds += numOfCellsCpy;
                                        dd10 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd10);
                                        dd10 += tmp;
                                        *current_dds = dd10;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD3 +++
                                         * +++++++++++
                                         */
                                        velComb = velY + velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[2] - gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd13 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd13);
                                        dd13 -= tmp;
                                        *current_dds = dd13;
                                        current_dds += numOfCellsCpy;
                                        dd12 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd12);
                                        dd12 += tmp;
                                        *current_dds = dd12;
                                        current_dds += numOfCellsCpy;
                                        velComb = velY - velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[2] + gravitationCpy.data[1]) * ((T)-1/(T)36) * rho;
                                        dd15 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd15);
                                        dd15 -= tmp;
                                        *current_dds = dd15;
                                        current_dds += numOfCellsCpy;
                                        dd14 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd14);
                                        dd14 += tmp;
                                        *current_dds = dd14;
                                        current_dds += numOfCellsCpy;
                                        
                                        /*
                                         * +++++++++++
                                         * +++ DD4 +++
                                         * +++++++++++
                                         */
                                        velSqr = velZ * velZ;
                                        tmp = gravitationCpy.data[2]*((T)1/(T)18) * rho;
                                        dd17 += tauInv * (eq_dd_a1(velZ, velSqr, dd_param) - dd17);
                                        dd17 -= tmp;
                                        *current_dds = dd17;
                                        current_dds += numOfCellsCpy;
                                        dd16 += tauInv * (eq_dd_a0(velZ, velSqr, dd_param) - dd16);
                                        dd16 += tmp;
                                        *current_dds = dd16;
                                        current_dds += numOfCellsCpy;
                                        dd18 += tauInv * (eq_dd18(dd_param) - dd18);
                                        *current_dds = dd18;

                                        break;
                                    case (OBSTACLE):
                                        if (storeVelocities)
                                        {
                                            velX = (T)0;
                                            velY = (T)0;
                                            velZ = (T)0;
                                        }
                                        
                                        break;
                                    case (VELOCITY_INJECTION):
                                        velX = drivenCavityVelocity;
                                        velY = (T)0;
                                        velZ = (T)0;
                                        rho = (T)1;
                                        dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);

                                        /*
                                         * +++++++++++
                                         * +++ DD0 +++
                                         * +++++++++++
                                         */
                                        velSqr = velX * velX;
                                        tmp = gravitationCpy.data[0] * ((T)1/(T)18) * rho;
                                        dd1 = eq_dd_a1(velX, velSqr, dd_param);
                                        dd1 -= tmp;
                                        *current_dds = dd1;
                                        current_dds += numOfCellsCpy;
                                        dd0 = eq_dd_a0(velX, velSqr, dd_param);
                                        dd0 += tmp;
                                        *current_dds = dd0;
                                        current_dds += numOfCellsCpy;
                                        velSqr = velY * velY;
                                        tmp = gravitationCpy.data[1] * ((T)-1/(T)18) * rho;
                                        dd3 = eq_dd_a1(velY, velSqr, dd_param);
                                        dd3 -= tmp;
                                        *current_dds = dd3;
                                        current_dds += numOfCellsCpy;
                                        dd2 = eq_dd_a0(velY, velSqr, dd_param);
                                        dd2 += tmp;
                                        *current_dds = dd2;
                                        current_dds += numOfCellsCpy;
                                        
                                        /*
                                         * +++++++++++
                                         * +++ DD1 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velY;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] - gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd5 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd5 -= tmp;
                                        *current_dds = dd5;
                                        current_dds += numOfCellsCpy;
                                        dd4 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd4 += tmp;
                                        *current_dds = dd4;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velY;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] + gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd7 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd7 -= tmp;
                                        *current_dds = dd7;
                                        current_dds += numOfCellsCpy;
                                        dd6 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd6 += tmp;
                                        *current_dds = dd6;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD2 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] + gravitationCpy.data[2]) * ((T)1/(T)36) * rho;
                                        dd9 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd9 -= tmp;
                                        *current_dds = dd9;
                                        current_dds += numOfCellsCpy;
                                        dd8 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd8 += tmp;
                                        *current_dds = dd8;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[0] - gravitationCpy.data[2]) * ((T)1/(T)36) * rho;
                                        dd11 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd11 -= tmp;
                                        *current_dds = dd11;
                                        current_dds += numOfCellsCpy;
                                        dd10 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd10 += tmp;
                                        *current_dds = dd10;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD3 +++
                                         * +++++++++++
                                         */
                                        velComb = velY + velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[2] - gravitationCpy.data[1]) * ((T)1/(T)36) * rho;
                                        dd13 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd13 -= tmp;
                                        *current_dds = dd13;
                                        current_dds += numOfCellsCpy;
                                        dd12 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd12 += tmp;
                                        *current_dds = dd12;
                                        current_dds += numOfCellsCpy;
                                        velComb = velY - velZ;
                                        velCombSqr = velComb * velComb;
                                        tmp = (gravitationCpy.data[2] + gravitationCpy.data[1]) * ((T)-1/(T)36) * rho;
                                        dd15 = eq_dd5(velComb, velCombSqr, dd_param);
                                        dd15 -= tmp;
                                        *current_dds = dd15;
                                        current_dds += numOfCellsCpy;
                                        dd14 = eq_dd4(velComb, velCombSqr, dd_param);
                                        dd14 += tmp;
                                        *current_dds = dd14;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD4 +++
                                         * +++++++++++
                                         */
                                        velSqr = velZ * velZ;
                                        tmp = gravitationCpy.data[2] * ((T)1/(T)18) * rho;
                                        dd17 = eq_dd_a1(velZ, velSqr, dd_param);
                                        dd17 -= tmp;
                                        *current_dds = dd17;
                                        current_dds += numOfCellsCpy;
                                        dd16 = eq_dd_a0(velZ, velSqr, dd_param);
                                        dd16 += tmp;
                                        *current_dds = dd16;
                                        current_dds += numOfCellsCpy;
                                        dd18 = eq_dd18(dd_param);
                                        *current_dds = dd18;
                                        
                                        break;
                                    case (GHOST_LAYER):
                                        break;
                                }

                                if (storeVelocities)
                                {
                                    velocities[id + 0 * numOfCellsCpy] = velX;
                                    velocities[id + 1 * numOfCellsCpy] = velY;
                                    velocities[id + 2 * numOfCellsCpy] = velZ;
                                }

                                if (storeDensities)
                                    densities[id] = rho;
                            }
                        }
                    }
                }
            }
        }
    }
}

template class CLbmAlphaCPU<double>;
template class CLbmAlphaCPU<float>;
