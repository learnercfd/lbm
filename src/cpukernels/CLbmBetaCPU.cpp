/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../libmath/CMath.hpp"
#include "CLbmBetaCPU.hpp"

template<class T>
CLbmBetaCPU<T>::CLbmBetaCPU(
        CVector<3, int> domainSize,
        CVector<3, T> gravitation) :
        domainSize(domainSize),
        gravitation(gravitation),
        numOfCells(domainSize.elements())
{
    deltaPos.data[0] = 1;
    deltaNeg.data[0] = numOfCells - 1;
    deltaPos.data[1] = domainSize.data[0];
    deltaNeg.data[1] = numOfCells - domainSize.data[0];
    deltaPos.data[2] = domainSize.data[0] * domainSize.data[1];
    deltaNeg.data[2] = numOfCells - domainSize.data[0] * domainSize.data[1];
}

template<class T>
CLbmBetaCPU<T>::~CLbmBetaCPU()
{
}

template<class T>
void CLbmBetaCPU<T>::betaKernelCPU(
        T* densityDistributions,
        Flag* flags,
        T* densities,
        T* velocities,
        const T tauInv,
        const T drivenCavityVelocity, 
        CVector<3, int> origin,
        CVector<3, int> size,
        CVector<3, int> blockDim,
        const bool storeDensities,
        const bool storeVelocities)
{
    CVector<3, int> domainSizeCpy = domainSize;
    int numOfCellsCpy = numOfCells;
    CVector<3, int> deltaPosCpy = deltaPos;
    CVector<3, int> deltaNegCpy = deltaNeg;
    
    int blockIdxLimZ = ((size.data[2] - 1) / blockDim.data[2]) + 1;
    for (int blockIdxZ = 0; blockIdxZ < blockIdxLimZ; blockIdxZ++)
    {
        int blockIdxLimY = ((size.data[1] - 1) / blockDim.data[1]) + 1;
        for (int blockIdxY = 0; blockIdxY < blockIdxLimY; blockIdxY++)
        {
            int blockIdxLimX = ((size.data[0] - 1) / blockDim.data[0]) + 1;
            for (int blockIdxX = 0; blockIdxX < blockIdxLimX; blockIdxX++)
            {
#pragma omp task default(none) firstprivate(domainSizeCpy, numOfCellsCpy, deltaPosCpy, deltaNegCpy, densityDistributions, flags, densities, velocities, tauInv, drivenCavityVelocity, origin, size, blockDim, storeDensities, storeVelocities, blockIdxX, blockIdxY, blockIdxZ)
                {
                    int elementIdxLimZ = CMath<int>::min(blockDim.data[2], size.data[2] - blockIdxZ * blockDim.data[2]);
                    for (int elementIdxZ = 0; elementIdxZ < elementIdxLimZ; elementIdxZ++)
                    {
                        int elementIdxLimY = CMath<int>::min(blockDim.data[1], size.data[1] - blockIdxY * blockDim.data[1]);
                        for (int elementIdxY = 0; elementIdxY < elementIdxLimY; elementIdxY++)
                        {
                            int elementIdxLimX = CMath<int>::min(blockDim.data[0], size.data[0] - blockIdxX * blockDim.data[0]);
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1600)
#pragma simd vecremainder
#endif
                            for (int elementIdxX = 0; elementIdxX < elementIdxLimX; elementIdxX++)
                            {
                                T dd0, dd1, dd2, dd3, dd4, dd5, dd6, dd7, dd8, dd9, dd10, dd11, dd12, dd13, dd14, dd15, dd16, dd17, dd18;
                                T rho;
                                T velX, velY, velZ;
                                T velSqr, velComb, velCombSqr;
                                T dd_param;
                                const int X = blockIdxX * blockDim.data[0] + elementIdxX;
                                const int Y = blockIdxY * blockDim.data[1] + elementIdxY;
                                const int Z = blockIdxZ * blockDim.data[2] + elementIdxZ;
                                const int id = (origin.data[2] + Z) * (domainSizeCpy.data[0] * domainSizeCpy.data[1]) + (origin.data[1] + Y) * domainSizeCpy.data[0] + (origin.data[0] + X);
                                const int dd0Offset = wrap(id + deltaNegCpy.data[0], numOfCellsCpy);
                                const int dd1Offset = wrap(id + deltaPosCpy.data[0], numOfCellsCpy);
                                const int dd2Offset = wrap(id + deltaNegCpy.data[1], numOfCellsCpy);
                                const int dd3Offset = wrap(id + deltaPosCpy.data[1], numOfCellsCpy);
                                const int dd4Offset = wrap(dd0Offset + deltaNegCpy.data[1], numOfCellsCpy);
                                const int dd5Offset = wrap(dd1Offset + deltaPosCpy.data[1], numOfCellsCpy);
                                const int dd6Offset = wrap(dd0Offset + deltaPosCpy.data[1], numOfCellsCpy);
                                const int dd7Offset = wrap(dd1Offset + deltaNegCpy.data[1], numOfCellsCpy);
                                const int dd8Offset = wrap(dd0Offset + deltaNegCpy.data[2], numOfCellsCpy);
                                const int dd9Offset = wrap(dd1Offset + deltaPosCpy.data[2], numOfCellsCpy);
                                const int dd10Offset = wrap(dd0Offset + deltaPosCpy.data[2], numOfCellsCpy);
                                const int dd11Offset = wrap(dd1Offset + deltaNegCpy.data[2], numOfCellsCpy);
                                const int dd12Offset = wrap(dd2Offset + deltaNegCpy.data[2], numOfCellsCpy);
                                const int dd13Offset = wrap(dd3Offset + deltaPosCpy.data[2], numOfCellsCpy);
                                const int dd14Offset = wrap(dd2Offset + deltaPosCpy.data[2], numOfCellsCpy);
                                const int dd15Offset = wrap(dd3Offset + deltaNegCpy.data[2], numOfCellsCpy);
                                const int dd16Offset = wrap(id + deltaNegCpy.data[2], numOfCellsCpy);
                                const int dd17Offset = wrap(id + deltaPosCpy.data[2], numOfCellsCpy);
                                T *current_dds = densityDistributions;

                                /*
                                 * +++++++++++
                                 * +++ DD0 +++
                                 * +++++++++++
                                 *
                                 * 0-3: f(1,0,0), f(-1,0,0),  f(0,1,0), f(0,-1,0)
                                 */
                                dd1 = current_dds[dd1Offset];
                                current_dds += numOfCellsCpy;
                                dd0 = current_dds[dd0Offset];
                                current_dds += numOfCellsCpy;
                                rho = dd0;
                                velX = dd0;
                                rho += dd1;
                                velX -= dd1;
                                dd3 = current_dds[dd3Offset];
                                current_dds += numOfCellsCpy;
                                dd2 = current_dds[dd2Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd2;
                                velY = dd2;
                                rho += dd3;
                                velY -= dd3;

                                /*
                                 * +++++++++++
                                 * +++ DD1 +++
                                 * +++++++++++
                                 *
                                 * 4-7: f(1,1,0), f(-1,-1,0), f(1,-1,0), f(-1,1,0)
                                 */
                                dd5 = current_dds[dd5Offset];
                                current_dds += numOfCellsCpy;
                                dd4 = current_dds[dd4Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd4;
                                velX += dd4;
                                velY += dd4;
                                rho += dd5;
                                velX -= dd5;
                                velY -= dd5;
                                dd7 = current_dds[dd7Offset];
                                current_dds += numOfCellsCpy;
                                dd6 = current_dds[dd6Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd6;
                                velX += dd6;
                                velY -= dd6;
                                rho += dd7;
                                velX -= dd7;
                                velY += dd7;

                                /*
                                 * +++++++++++
                                 * +++ DD2 +++
                                 * +++++++++++
                                 *
                                 * 8-11: f(1,0,1), f(-1,0,-1), f(1,0,-1), f(-1,0,1)
                                 */
                                dd9 = current_dds[dd9Offset];
                                current_dds += numOfCellsCpy;
                                dd8 = current_dds[dd8Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd8;
                                velX += dd8;
                                velZ = dd8;
                                rho += dd9;
                                velX -= dd9;
                                velZ -= dd9;
                                dd11 = current_dds[dd11Offset];
                                current_dds += numOfCellsCpy;
                                dd10 = current_dds[dd10Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd10;
                                velX += dd10;
                                velZ -= dd10;
                                rho += dd11;
                                velX -= dd11;
                                velZ += dd11;

                                /*
                                 * +++++++++++
                                 * +++ DD3 +++
                                 * +++++++++++
                                 *
                                 * 12-15: f(0,1,1), f(0,-1,-1), f(0,1,-1), f(0,-1,1)
                                 */
                                dd13 = current_dds[dd13Offset];
                                current_dds += numOfCellsCpy;
                                dd12 = current_dds[dd12Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd12;
                                velY += dd12;
                                velZ += dd12;
                                rho += dd13;
                                velY -= dd13;
                                velZ -= dd13;
                                dd15 = current_dds[dd15Offset];
                                current_dds += numOfCellsCpy;
                                dd14 = current_dds[dd14Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd14;
                                velY += dd14;
                                velZ -= dd14;
                                rho += dd15;
                                velY -= dd15;
                                velZ += dd15;

                                /*
                                 * +++++++++++
                                 * +++ DD4 +++
                                 * +++++++++++
                                 *
                                 * 16-18: f(0,0,1), f(0,0,-1),  f(0,0,0),  (not used)
                                 */
                                dd17 = current_dds[dd17Offset];
                                current_dds += numOfCellsCpy;
                                dd16 = current_dds[dd16Offset];
                                current_dds += numOfCellsCpy;
                                rho += dd16;
                                velZ += dd16;

                                rho += dd17;
                                velZ -= dd17;
                                dd18 = current_dds[id];
                                rho += dd18;
                                
                                current_dds = densityDistributions;

                                switch(flags[id])
                                {
                                    case (FLUID):
                                        dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);

                                        /*
                                         * +++++++++++
                                         * +++ DD0 +++
                                         * +++++++++++
                                         */
                                        velSqr = velX * velX;
                                        dd0 += tauInv * (eq_dd_a0(velX, velSqr, dd_param) - dd0);
                                        current_dds[dd1Offset] = dd0;
                                        current_dds += numOfCellsCpy;
                                        dd1 += tauInv * (eq_dd_a1(velX, velSqr, dd_param) - dd1);
                                        current_dds[dd0Offset] = dd1;
                                        current_dds += numOfCellsCpy;
                                        velSqr = velY * velY;
                                        dd2 += tauInv * (eq_dd_a0(velY, velSqr, dd_param) - dd2);
                                        current_dds[dd3Offset] = dd2;
                                        current_dds += numOfCellsCpy;
                                        dd3 += tauInv * (eq_dd_a1(velY, velSqr, dd_param) - dd3);
                                        current_dds[dd2Offset] = dd3;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD1 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velY;
                                        velCombSqr = velComb * velComb;
                                        dd4 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd4);
                                        current_dds[dd5Offset] = dd4;
                                        current_dds += numOfCellsCpy;
                                        dd5 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd5);
                                        current_dds[dd4Offset] = dd5;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velY;
                                        velCombSqr = velComb * velComb;
                                        dd6 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd6);
                                        current_dds[dd7Offset] = dd6;
                                        current_dds += numOfCellsCpy;
                                        dd7 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd7);
                                        current_dds[dd6Offset] = dd7;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD2 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velZ;
                                        velCombSqr = velComb * velComb;
                                        dd8 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd8);
                                        current_dds[dd9Offset] = dd8;
                                        current_dds += numOfCellsCpy;
                                        dd9 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd9);
                                        current_dds[dd8Offset] = dd9;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velZ;
                                        velCombSqr = velComb * velComb;
                                        dd10 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd10);
                                        current_dds[dd11Offset] = dd10;
                                        current_dds += numOfCellsCpy;
                                        dd11 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd11);
                                        current_dds[dd10Offset] = dd11;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD3 +++
                                         * +++++++++++
                                         */
                                        velComb = velY + velZ;
                                        velCombSqr = velComb * velComb;
                                        dd12 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd12);
                                        current_dds[dd13Offset] = dd12;
                                        current_dds += numOfCellsCpy;
                                        dd13 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd13);
                                        current_dds[dd12Offset] = dd13;
                                        current_dds += numOfCellsCpy;
                                        velComb = velY - velZ;
                                        velCombSqr = velComb * velComb;
                                        dd14 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd14);
                                        current_dds[dd15Offset] = dd14;
                                        current_dds += numOfCellsCpy;
                                        dd15 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd15);
                                        current_dds[dd14Offset] = dd15;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD4 +++
                                         * +++++++++++
                                         */
                                        velSqr = velZ * velZ;
                                        dd16 += tauInv * (eq_dd_a0(velZ, velSqr, dd_param) - dd16);
                                        current_dds[dd17Offset] = dd16;
                                        current_dds += numOfCellsCpy;
                                        dd17 += tauInv * (eq_dd_a1(velZ, velSqr, dd_param) - dd17);
                                        current_dds[dd16Offset] = dd17;
                                        current_dds += numOfCellsCpy;
                                        dd18 += tauInv * (eq_dd18(dd_param) - dd18);
                                        current_dds[id] = dd18;
                                        
                                        break;
                                    case (OBSTACLE):
                                        if (storeVelocities)
                                        {
                                            velX = (T)0;
                                            velY = (T)0;
                                            velZ = (T)0;
                                        }

                                        velSqr = dd1;    dd1 = dd0;      dd0 = velSqr;
                                        velSqr = dd3;    dd3 = dd2;      dd2 = velSqr;
                                        velSqr = dd5;    dd5 = dd4;      dd4 = velSqr;
                                        velSqr = dd7;    dd7 = dd6;      dd6 = velSqr;
                                        velSqr = dd9;    dd9 = dd8;      dd8 = velSqr;
                                        velSqr = dd11;   dd11 = dd10;    dd10 = velSqr;
                                        velSqr = dd13;   dd13 = dd12;    dd12 = velSqr;
                                        velSqr = dd15;   dd15 = dd14;    dd14 = velSqr;
                                        velSqr = dd17;   dd17 = dd16;    dd16 = velSqr;

                                        break;
                                    case (VELOCITY_INJECTION):
                                        velX = drivenCavityVelocity;
                                        velY = (T)0;
                                        velZ = (T)0;
                                        rho = (T)1;
                                        
                                        /*
                                         * +++++++++++
                                         * +++ DD0 +++
                                         * +++++++++++
                                         */
                                        dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);
                                        velSqr = velX * velX;
                                        dd0 = eq_dd_a0(velX, velSqr, dd_param);
                                        current_dds[dd1Offset] = dd0;
                                        current_dds += numOfCellsCpy;
                                        dd1 = eq_dd_a1(velX, velSqr, dd_param);
                                        current_dds[dd0Offset] = dd1;
                                        current_dds += numOfCellsCpy;
                                        velSqr = velY * velY;
                                        dd2 = eq_dd_a0(velY, velSqr, dd_param);
                                        current_dds[dd3Offset] = dd2;
                                        current_dds += numOfCellsCpy;
                                        dd3 = eq_dd_a1(velY, velSqr, dd_param);
                                        current_dds[dd2Offset] = dd3;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD1 +++
                                         * +++++++++++
                                         */
                                        velComb = velX + velY;
                                        velCombSqr = velComb * velComb;
                                        dd4 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd5Offset] = dd4;
                                        current_dds += numOfCellsCpy;
                                        dd5 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd4Offset] = dd5;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velY;
                                        velCombSqr = velComb * velComb;
                                        dd6 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd7Offset] = dd6;
                                        current_dds += numOfCellsCpy;
                                        dd7 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd6Offset] = dd7;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD2 +++
                                         * +++++++++++
                                         */
                                        velComb = velX+velZ;
                                        velCombSqr = velComb * velComb;
                                        dd8 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd9Offset] = dd8;
                                        current_dds += numOfCellsCpy;
                                        dd9 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd8Offset] = dd9;
                                        current_dds += numOfCellsCpy;
                                        velComb = velX - velZ;
                                        velCombSqr = velComb * velComb;
                                        dd10 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd11Offset] = dd10;
                                        current_dds += numOfCellsCpy;
                                        dd11 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd10Offset] = dd11;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD3 +++
                                         * +++++++++++
                                         */
                                        velComb = velY + velZ;
                                        velCombSqr = velComb * velComb;
                                        dd12 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd13Offset] = dd12;
                                        current_dds += numOfCellsCpy;
                                        dd13 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd12Offset] = dd13;
                                        current_dds += numOfCellsCpy;
                                        velComb = velY - velZ;
                                        velCombSqr = velComb * velComb;
                                        dd14 = eq_dd4(velComb, velCombSqr, dd_param);
                                        current_dds[dd15Offset] = dd14;
                                        current_dds += numOfCellsCpy;
                                        dd15 = eq_dd5(velComb, velCombSqr, dd_param);
                                        current_dds[dd14Offset] = dd15;
                                        current_dds += numOfCellsCpy;

                                        /*
                                         * +++++++++++
                                         * +++ DD4 +++
                                         * +++++++++++
                                         */
                                        velSqr = velZ * velZ;
                                        dd16 = eq_dd_a0(velZ, velSqr, dd_param);
                                        current_dds[dd17Offset] = dd16;
                                        current_dds += numOfCellsCpy;
                                        dd17 = eq_dd_a1(velZ, velSqr, dd_param);
                                        current_dds[dd16Offset] = dd17;
                                        current_dds += numOfCellsCpy;
                                        dd18 = eq_dd18(dd_param);
                                        current_dds[id] = dd18;
                                        
                                        break;
                                    case (GHOST_LAYER):
                                        break;
                                }

                                if (storeVelocities)
                                {
                                    velocities[id + 0 * numOfCellsCpy] = velX;
                                    velocities[id + 1 * numOfCellsCpy] = velY;
                                    velocities[id + 2 * numOfCellsCpy] = velZ;
                                }

                                if (storeDensities)
                                {
                                    densities[id] = rho;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

template class CLbmBetaCPU<double>;
template class CLbmBetaCPU<float>;
