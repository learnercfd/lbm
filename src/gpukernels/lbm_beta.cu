/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lbm_beta.cuh"

template<typename T>
__global__ void lbm_kernel_beta(
        T* densityDistributions,
        const Flag* flags,
        T* velocities,
        T* densities,
        const T tauInv,
        const T gravitationX,
        const T gravitationY,
        const T gravitationZ,
        const T drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities)
{
    const unsigned int numOfCells = domainCellsX * domainCellsY * domainCellsZ;
    const unsigned int X = blockIdx.x * blockDim.x + threadIdx.x;
    const unsigned int Y = blockIdx.y * blockDim.y + threadIdx.y;
    const unsigned int Z = blockIdx.z * blockDim.z + threadIdx.z;
    const unsigned int id = (originZ + Z) * (domainCellsX * domainCellsY) + (originY + Y) * domainCellsX + (originX + X);

    if (id >= numOfCells)
        return;

    if (X >= sizeX || Y >= sizeY || Z >= sizeZ)
        return;

    T velX, velY, velZ;
    T dd0, dd1, dd2, dd3, dd4, dd5, dd6, dd7, dd8, dd9, dd10, dd11, dd12, dd13, dd14, dd15, dd16, dd17, dd18;
    T rho;
    T velSqr, velComb, velCombSqr;
    T dd_param;
    const int DELTA_POS_X = 1;
    const int DELTA_NEG_X = numOfCells - 1;
    const int DELTA_POS_Y = domainCellsX;
    const int DELTA_NEG_Y = numOfCells - domainCellsX;
    const int DELTA_POS_Z = domainCellsX * domainCellsY;
    const int DELTA_NEG_Z = numOfCells - domainCellsX * domainCellsY;
    const Flag flag = flags[id];
    T *current_dds = densityDistributions;

    /*
     * +++++++++++
     * +++ DD0 +++
     * +++++++++++
     *
     * 0-3: f(1,0,0), f(-1,0,0),  f(0,1,0), f(0,-1,0)
     */
    dd1 = current_dds[wrap(id + DELTA_POS_X, numOfCells)];
    current_dds += numOfCells;
    dd0 = current_dds[wrap(id + DELTA_NEG_X, numOfCells)];
    current_dds += numOfCells;
    rho = dd0;
    velX = dd0;
    rho += dd1;
    velX -= dd1;
    dd3 = current_dds[wrap(id + DELTA_POS_Y, numOfCells)];
    current_dds += numOfCells;
    dd2 = current_dds[wrap(id + DELTA_NEG_Y, numOfCells)];
    current_dds += numOfCells;
    rho += dd2;
    velY = dd2;
    rho += dd3;
    velY -= dd3;

    /*
     * +++++++++++
     * +++ DD1 +++
     * +++++++++++
     *
     * 4-7: f(1,1,0), f(-1,-1,0), f(1,-1,0), f(-1,1,0)
     */
    dd5 = current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Y, numOfCells)];
    current_dds += numOfCells;
    dd4 = current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Y, numOfCells)];
    current_dds += numOfCells;
    rho += dd4;
    velX += dd4;
    velY += dd4;
    rho += dd5;
    velX -= dd5;
    velY -= dd5;
    dd7 = current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Y, numOfCells)];
    current_dds += numOfCells;
    dd6 = current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Y, numOfCells)];
    current_dds += numOfCells;
    rho += dd6;
    velX += dd6;
    velY -= dd6;
    rho += dd7;
    velX -= dd7;
    velY += dd7;

    /*
     * +++++++++++
     * +++ DD2 +++
     * +++++++++++
     *
     * 8-11: f(1,0,1), f(-1,0,-1), f(1,0,-1), f(-1,0,1)
     */
    dd9 = current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Z, numOfCells)];
    current_dds += numOfCells;
    dd8 = current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Z, numOfCells)];
    current_dds += numOfCells;
    rho += dd8;
    velX += dd8;
    velZ = dd8;
    rho += dd9;
    velX -= dd9;
    velZ -= dd9;
    dd11 = current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Z, numOfCells)];
    current_dds += numOfCells;
    dd10 = current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Z, numOfCells)];
    current_dds += numOfCells;
    rho += dd10;
    velX += dd10;
    velZ -= dd10;
    rho += dd11;
    velX -= dd11;
    velZ += dd11;

    /*
     * +++++++++++
     * +++ DD3 +++
     * +++++++++++
     *
     * 12-15: f(0,1,1), f(0,-1,-1), f(0,1,-1), f(0,-1,1)
     */
    dd13 = current_dds[wrap(id + DELTA_POS_Y + DELTA_POS_Z, numOfCells)];
    current_dds += numOfCells;
    dd12 = current_dds[wrap(id + DELTA_NEG_Y + DELTA_NEG_Z, numOfCells)];
    current_dds += numOfCells;
    rho += dd12;
    velY += dd12;
    velZ += dd12;
    rho += dd13;
    velY -= dd13;
    velZ -= dd13;
    dd15 = current_dds[wrap(id + DELTA_POS_Y + DELTA_NEG_Z, numOfCells)];
    current_dds += numOfCells;
    dd14 = current_dds[wrap(id + DELTA_NEG_Y + DELTA_POS_Z, numOfCells)];
    current_dds += numOfCells;
    rho += dd14;
    velY += dd14;
    velZ -= dd14;
    rho += dd15;
    velY -= dd15;
    velZ += dd15;

    /*
     * +++++++++++
     * +++ DD4 +++
     * +++++++++++
     *
     * 16-18: f(0,0,1), f(0,0,-1), f(0,0,0)
     */
    dd17 = current_dds[wrap(id + DELTA_POS_Z, numOfCells)];
    current_dds += numOfCells;
    dd16 = current_dds[wrap(id + DELTA_NEG_Z, numOfCells)];
    current_dds += numOfCells;
    rho += dd16;
    velZ += dd16;
    rho += dd17;
    velZ -= dd17;
    dd18 = current_dds[id];
    rho += dd18;

    current_dds = densityDistributions;

    switch(flag)
    {
        case (FLUID):
            dd_param = rho - ((T)3/(T)2)*(velX*velX + velY*velY + velZ*velZ);

            /*
             * +++++++++++
             * +++ DD0 +++
             * +++++++++++
             */
            velSqr = velX * velX;
            dd0 += tauInv * (eq_dd_a0(velX, velSqr, dd_param) - dd0);
            current_dds[wrap(id + DELTA_POS_X, numOfCells)] = dd0;
            current_dds += numOfCells;
            dd1 += tauInv * (eq_dd_a1(velX, velSqr, dd_param) - dd1);
            current_dds[wrap(id + DELTA_NEG_X, numOfCells)] = dd1;
            current_dds += numOfCells;
            velSqr = velY * velY;
            dd2 += tauInv * (eq_dd_a0(velY, velSqr, dd_param) - dd2);
            current_dds[wrap(id + DELTA_POS_Y, numOfCells)] = dd2;
            current_dds += numOfCells;
            dd3 += tauInv * (eq_dd_a1(velY, velSqr, dd_param) - dd3);
            current_dds[wrap(id + DELTA_NEG_Y, numOfCells)] = dd3;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD1 +++
             * +++++++++++
             */
            velComb = velX + velY;
            velCombSqr = velComb * velComb;
            dd4 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd4);
            current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Y, numOfCells)] = dd4;
            current_dds += numOfCells;
            dd5 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd5);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Y, numOfCells)] = dd5;
            current_dds += numOfCells;
            velComb = velX - velY;
            velCombSqr = velComb * velComb;
            dd6 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd6);
            current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Y, numOfCells)] = dd6;
            current_dds += numOfCells;
            dd7 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd7);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Y, numOfCells)] = dd7;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD2 +++
             * +++++++++++
             */
            velComb = velX + velZ;
            velCombSqr = velComb * velComb;
            dd8 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd8);
            current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Z, numOfCells)] = dd8;
            current_dds += numOfCells;
            dd9 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd9);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Z, numOfCells)] = dd9;
            current_dds += numOfCells;
            velComb = velX - velZ;
            velCombSqr = velComb * velComb;
            dd10 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd10);
            current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Z, numOfCells)] = dd10;
            current_dds += numOfCells;
            dd11 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd11);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Z, numOfCells)] = dd11;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD3 +++
             * +++++++++++
             */
            velComb = velY + velZ;
            velCombSqr = velComb * velComb;
            dd12 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd12);
            current_dds[wrap(id + DELTA_POS_Y + DELTA_POS_Z, numOfCells)] = dd12;
            current_dds += numOfCells;
            dd13 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd13);
            current_dds[wrap(id + DELTA_NEG_Y + DELTA_NEG_Z, numOfCells)] = dd13;
            current_dds += numOfCells;
            velComb = velY - velZ;
            velCombSqr = velComb * velComb;
            dd14 += tauInv * (eq_dd4(velComb, velCombSqr, dd_param) - dd14);
            current_dds[wrap(id + DELTA_POS_Y + DELTA_NEG_Z, numOfCells)] = dd14;
            current_dds += numOfCells;
            dd15 += tauInv * (eq_dd5(velComb, velCombSqr, dd_param) - dd15);
            current_dds[wrap(id + DELTA_NEG_Y + DELTA_POS_Z, numOfCells)] = dd15;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD4 +++
             * +++++++++++
             */
            velSqr = velZ * velZ;
            dd16 += tauInv * (eq_dd_a0(velZ, velSqr, dd_param) - dd16);
            current_dds[wrap(id + DELTA_POS_Z, numOfCells)] = dd16;
            current_dds += numOfCells;
            dd17 += tauInv * (eq_dd_a1(velZ, velSqr, dd_param) - dd17);
            current_dds[wrap(id + DELTA_NEG_Z, numOfCells)] = dd17;
            current_dds += numOfCells;
            dd18 += tauInv * (eq_dd18(dd_param) - dd18);
            current_dds[id] = dd18;

            break;
        case (OBSTACLE):
            if (storeVelocities)
            {
                velX = (T)0;
                velY = (T)0;
                velZ = (T)0;
            }

            velSqr = dd1;    dd1 = dd0;      dd0 = velSqr;
            velSqr = dd3;    dd3 = dd2;      dd2 = velSqr;
            velSqr = dd5;    dd5 = dd4;      dd4 = velSqr;
            velSqr = dd7;    dd7 = dd6;      dd6 = velSqr;
            velSqr = dd9;    dd9 = dd8;      dd8 = velSqr;
            velSqr = dd11;   dd11 = dd10;    dd10 = velSqr;
            velSqr = dd13;   dd13 = dd12;    dd12 = velSqr;
            velSqr = dd15;   dd15 = dd14;    dd14 = velSqr;
            velSqr = dd17;   dd17 = dd16;    dd16 = velSqr;

            break;
        case (VELOCITY_INJECTION):
            velX = drivenCavityVelocity;
            velY = (T)0;
            velZ = (T)0;
            rho = (T)1;
            dd_param = rho - ((T)3/(T)2) * (velX * velX + velY * velY + velZ * velZ);

            /*
             * +++++++++++
             * +++ DD0 +++
             * +++++++++++
             */
            velSqr = velX * velX;
            dd0 = eq_dd_a0(velX, velSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_X, numOfCells)] = dd0;
            current_dds += numOfCells;
            dd1 = eq_dd_a1(velX, velSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_X, numOfCells)] = dd1;
            current_dds += numOfCells;
            velSqr = velY * velY;
            dd2 = eq_dd_a0(velY, velSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_Y, numOfCells)] = dd2;
            current_dds += numOfCells;
            dd3 = eq_dd_a1(velY, velSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_Y, numOfCells)] = dd3;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD1 +++
             * +++++++++++
             */
            velComb = velX + velY;
            velCombSqr = velComb * velComb;
            dd4 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Y, numOfCells)] = dd4;
            current_dds += numOfCells;
            dd5 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Y, numOfCells)] = dd5;
            current_dds += numOfCells;
            velComb = velX - velY;
            velCombSqr = velComb * velComb;
            dd6 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Y, numOfCells)] = dd6;
            current_dds += numOfCells;
            dd7 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Y, numOfCells)] = dd7;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD2 +++
             * +++++++++++
             */
            velComb = velX+velZ;
            velCombSqr = velComb * velComb;
            dd8 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_X + DELTA_POS_Z, numOfCells)] = dd8;
            current_dds += numOfCells;
            dd9 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_NEG_Z, numOfCells)] = dd9;
            current_dds += numOfCells;
            velComb = velX - velZ;
            velCombSqr = velComb * velComb;
            dd10 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_X + DELTA_NEG_Z, numOfCells)] = dd10;
            current_dds += numOfCells;
            dd11 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_X + DELTA_POS_Z, numOfCells)] = dd11;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD3 +++
             * +++++++++++
             */
            velComb = velY + velZ;
            velCombSqr = velComb * velComb;
            dd12 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_Y + DELTA_POS_Z, numOfCells)] = dd12;
            current_dds += numOfCells;
            dd13 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_Y + DELTA_NEG_Z, numOfCells)] = dd13;
            current_dds += numOfCells;
            velComb = velY - velZ;
            velCombSqr = velComb * velComb;
            dd14 = eq_dd4(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_Y + DELTA_NEG_Z, numOfCells)] = dd14;
            current_dds += numOfCells;
            dd15 = eq_dd5(velComb, velCombSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_Y + DELTA_POS_Z, numOfCells)] = dd15;
            current_dds += numOfCells;

            /*
             * +++++++++++
             * +++ DD4 +++
             * +++++++++++
             */
            velSqr = velZ * velZ;
            dd16 = eq_dd_a0(velZ, velSqr, dd_param);
            current_dds[wrap(id + DELTA_POS_Z, numOfCells)] = dd16;
            current_dds += numOfCells;
            dd17 = eq_dd_a1(velZ, velSqr, dd_param);
            current_dds[wrap(id + DELTA_NEG_Z, numOfCells)] = dd17;
            current_dds += numOfCells;
            dd18 = eq_dd18(dd_param);
            current_dds[id] = dd18;

            break;
        case (GHOST_LAYER):
            break;
    }

    if (flag == GHOST_LAYER)
        return;

    if (storeVelocities)
    {
        current_dds = &velocities[id];
        *current_dds = velX;
        current_dds += numOfCells;
        *current_dds = velY;
        current_dds += numOfCells;
        *current_dds = velZ;
    }

    if (storeDensities)
        densities[id] = rho;
}

template __global__ void lbm_kernel_beta<float>(
        float *densityDistributions,
        const Flag *flags,
        float *velocities,
        float *densities,
        const float tauInv,
        const float gravitationX,
        const float gravitationY,
        const float gravitationZ,
        const float drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities);
template __global__ void lbm_kernel_beta<double>(
        double *densityDistributions,
        const Flag *flags,
        double *velocities,
        double *densities,
        const double tauInv,
        const double gravitationX,
        const double gravitationY,
        const double gravitationZ,
        const double drivenCavityVelocity,
        const int originX,
        const int originY,
        const int originZ,
        const int sizeX,
        const int sizeY,
        const int sizeZ,
        const int domainCellsX,
        const int domainCellsY,
        const int domainCellsZ,
        const bool storeDensities,
        const bool storeVelocities);
