/*
 * Copyright
 * 2010 Martin Schreiber
 * 2013 Arash Bakhtiari
 * 2016 Christoph Riesinger, Ayman Saleem
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CLbmVisualizationNetCDF.hpp"

#if defined(USE_MPI) && defined(PAR_NETCDF)
#include <mpi.h>
#endif

#include <cstdio>
#include <typeinfo>

template <class T>
CLbmVisualizationNetCDF<T>::CLbmVisualizationNetCDF(
        int id,
        int visualizationRate,
        CLbmSolverCPU<T>* solverCPU,
        CLbmSolverGPU<T>* solverGPU,
#if defined(USE_MPI) && defined(PAR_NETCDF)
        CVector<3, int> numOfSubdomains,
#endif
        std::string filePath) :
        CLbmVisualization<T>(id, visualizationRate, solverCPU, solverGPU),
#if defined(USE_MPI) && defined(PAR_NETCDF)
        numOfSubdomains(numOfSubdomains),
#endif
        filePath(filePath)
{
}

template <class T>
void CLbmVisualizationNetCDF<T>::openFile(int iteration)
{
    int error;
    std::stringstream fileName;
#if defined(USE_MPI) && defined(PAR_NETCDF)
    fileName << filePath << "/visualization_" << iteration << ".nc";
    error = nc_create_par(fileName.str().c_str(), NC_CLOBBER | NC_NETCDF4 | NC_MPIIO, MPI_COMM_WORLD, MPI_INFO_NULL, &fileId);
#else
    fileName << filePath << "/visualization_" << id << "_" << iteration << ".nc";
    error = nc_create(fileName.str().c_str(), NC_CLOBBER, &fileId);
#endif

    if (error)
    {
        std::cerr << "----- CLbmVisualizationNetCDF<T>::openFile() -----" << std::endl;
#if defined(USE_MPI) && defined(PAR_NETCDF)
        std::cerr << "An error occurred while opening parallel netCDF file \"" << fileName.str() << "\"" << std::endl;std::cerr << nc_strerror(error) << std::endl;
#else
        std::cerr << "An error occurred while opening serial netCDF file \"" << fileName.str() << "\"" << std::endl;std::cerr << nc_strerror(error) << std::endl;
#endif
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "--------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationNetCDF<T>::closeFile()
{
    int error;
    error = nc_close(fileId);

    if (error)
    {
        std::cerr << "----- CLbmVisualizationNetCDF<T>::closeFile() -----" << std::endl;
        std::cerr << "An error occurred while closing parallel netCDF file" << std::endl;
        std::cerr << nc_strerror(error) << std::endl;
        std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
        std::cerr << "---------------------------------------------------" << std::endl;

        exit (EXIT_FAILURE);
    }
}

template <class T>
void CLbmVisualizationNetCDF<T>::defineData()
{
    const std::string DIM_UNIT("meters");
    const std::string FLAG_UNIT("flag");
    const std::string DENSITY_UNIT("kg/m^3");
    const std::string VELOCITY_UNIT("m/s");
    int dimIds[3];
    int oldFill;

    /*
     * Due to the strange fact that netCDF takes the first coordinate as
     * the fastest running, dimensions have to be inverted. On the other
     * hand, since that's exactly the way how our data is stored, no final
     * matrix inversion is necessary.
     */
#if defined(USE_MPI) && defined(PAR_NETCDF)
    nc_def_dim(fileId, "x", numOfSubdomains[0] *  solverCPU->getDomain()->getSize()[0], &dimIds[2]);
    nc_def_dim(fileId, "y", numOfSubdomains[1] * (solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1]), &dimIds[1]);
    nc_def_dim(fileId, "z", numOfSubdomains[2] *  solverCPU->getDomain()->getSize()[2], &dimIds[0]);
#else
    nc_def_dim(fileId, "x", solverCPU->getDomain()->getSize()[0], &dimIds[2]);
    nc_def_dim(fileId, "y", solverCPU->getDomain()->getSize()[1] + solverGPU->getDomain()->getSize()[1], &dimIds[1]);
    nc_def_dim(fileId, "z", solverCPU->getDomain()->getSize()[2], &dimIds[0]);
#endif

    nc_def_var(fileId, "x", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 1, &dimIds[2], &dimVarIds[2]);
    nc_def_var(fileId, "y", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 1, &dimIds[1], &dimVarIds[1]);
    nc_def_var(fileId, "z", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 1, &dimIds[0], &dimVarIds[0]);
    nc_def_var(fileId, "flags", NC_INT, 3, dimIds, &flagsVarId);
    nc_def_var(fileId, "densities", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 3, dimIds, &densitiesVarId);
    nc_def_var(fileId, "velocities_x", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 3, dimIds, &velocitiesVarId[0]);
    nc_def_var(fileId, "velocities_y", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 3, dimIds, &velocitiesVarId[1]);
    nc_def_var(fileId, "velocities_z", ((typeid(T) == typeid(float)) ? NC_FLOAT : NC_DOUBLE), 3, dimIds, &velocitiesVarId[2]);

    nc_put_att_text(fileId, dimVarIds[0], "units", DIM_UNIT.length(), DIM_UNIT.c_str());
    nc_put_att_text(fileId, dimVarIds[1], "units", DIM_UNIT.length(), DIM_UNIT.c_str());
    nc_put_att_text(fileId, dimVarIds[2], "units", DIM_UNIT.length(), DIM_UNIT.c_str());
    nc_put_att_text(fileId, flagsVarId, "units", FLAG_UNIT.length(), FLAG_UNIT.c_str());
    nc_put_att_text(fileId, densitiesVarId, "units", DENSITY_UNIT.length(), DENSITY_UNIT.c_str());
    nc_put_att_text(fileId, velocitiesVarId[0], "units", VELOCITY_UNIT.length(), VELOCITY_UNIT.c_str());
    nc_put_att_text(fileId, velocitiesVarId[1], "units", VELOCITY_UNIT.length(), VELOCITY_UNIT.c_str());
    nc_put_att_text(fileId, velocitiesVarId[2], "units", VELOCITY_UNIT.length(), VELOCITY_UNIT.c_str());

    /*
    nc_def_var_deflate(fileId, flagsVarId, NC_SHUFFLE, true, DEFLATE_LEVEL);
    nc_def_var_deflate(fileId, densitiesVarId, NC_SHUFFLE, true, DEFLATE_LEVEL);
    nc_def_var_deflate(fileId, velocitiesVarId[0], NC_SHUFFLE, true, DEFLATE_LEVEL);
    nc_def_var_deflate(fileId, velocitiesVarId[1], NC_SHUFFLE, true, DEFLATE_LEVEL);
    nc_def_var_deflate(fileId, velocitiesVarId[2], NC_SHUFFLE, true, DEFLATE_LEVEL);
    */

    nc_set_fill(fileId, NC_NOFILL, &oldFill);

    nc_enddef(fileId);

#if defined(USE_MPI) && defined(PAR_NETCDF)
    nc_var_par_access(fileId, flagsVarId, NC_INDEPENDENT);
    nc_var_par_access(fileId, densitiesVarId, NC_INDEPENDENT);
    nc_var_par_access(fileId, velocitiesVarId[0], NC_INDEPENDENT);
    nc_var_par_access(fileId, velocitiesVarId[1], NC_INDEPENDENT);
    nc_var_par_access(fileId, velocitiesVarId[2], NC_INDEPENDENT);
#endif
}

template <class T>
void CLbmVisualizationNetCDF<T>::writeData()
{
    CVector<3, int> size(solverCPU->getDomain()->getSize());
    size[1] += solverGPU->getDomain()->getSize()[1];
    CVector<3, T> length(solverCPU->getDomain()->getLength());
    length[1] += solverGPU->getDomain()->getLength()[1];

    if (id == 0)
    {
#if defined(USE_MPI) && defined(PAR_NETCDF)
        T x[numOfSubdomains[0] * size[0]];
        T y[numOfSubdomains[1] * size[1]];
        T z[numOfSubdomains[2] * size[2]];
#else
        T x[size[0]];
        T y[size[1]];
        T z[size[2]];
#endif

#if defined(USE_MPI) && defined(PAR_NETCDF)
        for (int i = 0; i < numOfSubdomains[0] * size[0]; i++)
#else
        for (int i = 0; i < size[0]; i++)
#endif
        {
            x[i] = ((T)i + (T)0.5) * length[0] / (T)size[0];
        }
#if defined(USE_MPI) && defined(PAR_NETCDF)
        for (int i = 0; i < numOfSubdomains[1] * size[1]; i++)
#else
        for (int i = 0; i < size[1]; i++)
#endif
        {
            y[i] = ((T)i + (T)0.5) * length[1] / (T)size[1];
        }
#if defined(USE_MPI) && defined(PAR_NETCDF)
        for (int i = 0; i < numOfSubdomains[2] * size[2]; i++)
#else
        for (int i = 0; i < size[2]; i++)
#endif
        {
            z[i] = ((T)i + (T)0.5) * length[2] / (T)size[2];
        }

        if (typeid(T) == typeid(double))
        {
            nc_put_var_double(fileId, dimVarIds[2], (double*)x);
            nc_put_var_double(fileId, dimVarIds[1], (double*)y);
            nc_put_var_double(fileId, dimVarIds[0], (double*)z);
        } else if(typeid(T) == typeid(float)) {
            nc_put_var_float(fileId, dimVarIds[2], (float*)x);
            nc_put_var_float(fileId, dimVarIds[1], (float*)y);
            nc_put_var_float(fileId, dimVarIds[0], (float*)z);
        } else {
            std::cerr << "----- CLbmVisualizationNetCDF<T>::writeData() -----" << std::endl;
            std::cerr << "A datatype was specified (neither float nor double) which is not supported" << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }

    if (solverCPU->getDomain()->hasDomain())
    {
        size_t start[3] = {
                static_cast<size_t>(solverCPU->getDomain()->getOrigin()[2]),
                static_cast<size_t>(solverCPU->getDomain()->getOrigin()[1]),
                static_cast<size_t>(solverCPU->getDomain()->getOrigin()[0])};
        size_t count[3] = {
                static_cast<size_t>(solverCPU->getDomain()->getSize()[2]),
                static_cast<size_t>(solverCPU->getDomain()->getSize()[1]),
                static_cast<size_t>(solverCPU->getDomain()->getSize()[0])};

        solverCPU->getFlags(CPU, flagsCPU);
        solverCPU->getDensities(CPU, densitiesCPU);
        solverCPU->getVelocities(CPU, velocitiesCPU);

        nc_put_vara_int(fileId, flagsVarId, start, count, (int*)flagsCPU);

        if (typeid(T) == typeid(double))
        {
            nc_put_vara_double(fileId, densitiesVarId, start, count, (double*)densitiesCPU);
            nc_put_vara_double(fileId, velocitiesVarId[0], start, count, (double*)&(velocitiesCPU[0]));
            nc_put_vara_double(fileId, velocitiesVarId[1], start, count, (double*)&(velocitiesCPU[solverCPU->getDomain()->getNumOfCells()]));
            nc_put_vara_double(fileId, velocitiesVarId[2], start, count, (double*)&(velocitiesCPU[2 * solverCPU->getDomain()->getNumOfCells()]));
        } else if(typeid(T) == typeid(float)) {
            nc_put_vara_float(fileId, densitiesVarId, start, count, (float*)densitiesCPU);
            nc_put_vara_float(fileId, velocitiesVarId[0], start, count, (float*)&(velocitiesCPU[0]));
            nc_put_vara_float(fileId, velocitiesVarId[1], start, count, (float*)&(velocitiesCPU[solverCPU->getDomain()->getNumOfCells()]));
            nc_put_vara_float(fileId, velocitiesVarId[2], start, count, (float*)&(velocitiesCPU[2 * solverCPU->getDomain()->getNumOfCells()]));
        } else {
            std::cerr << "----- CLbmVisualizationNetCDF<T>::writeData() -----" << std::endl;
            std::cerr << "A datatype was specified (neither float nor double) which is not supported" << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
    if (solverGPU->getDomain()->hasDomain())
    {
        size_t start[3] = {
                static_cast<size_t>(solverGPU->getDomain()->getOrigin()[2]),
                static_cast<size_t>(solverGPU->getDomain()->getOrigin()[1]),
                static_cast<size_t>(solverGPU->getDomain()->getOrigin()[0])};
        size_t count[3] = {
                static_cast<size_t>(solverGPU->getDomain()->getSize()[2]),
                static_cast<size_t>(solverGPU->getDomain()->getSize()[1]),
                static_cast<size_t>(solverGPU->getDomain()->getSize()[0])};

        solverGPU->getFlags(CPU, flagsGPU);
        solverGPU->getDensities(CPU, densitiesGPU);
        solverGPU->getVelocities(CPU, velocitiesGPU);

        nc_put_vara_int(fileId, flagsVarId, start, count, (int*)flagsGPU);

        if (typeid(T) == typeid(double))
        {
            nc_put_vara_double(fileId, densitiesVarId, start, count, (double*)densitiesGPU);
            nc_put_vara_double(fileId, velocitiesVarId[0], start, count, (double*)&(velocitiesGPU[0]));
            nc_put_vara_double(fileId, velocitiesVarId[1], start, count, (double*)&(velocitiesGPU[solverGPU->getDomain()->getNumOfCells()]));
            nc_put_vara_double(fileId, velocitiesVarId[2], start, count, (double*)&(velocitiesGPU[2 * solverGPU->getDomain()->getNumOfCells()]));
        } else if(typeid(T) == typeid(float)) {
            nc_put_vara_float(fileId, densitiesVarId, start, count, (float*)densitiesGPU);
            nc_put_vara_float(fileId, velocitiesVarId[0], start, count, (float*)&(velocitiesGPU[0]));
            nc_put_vara_float(fileId, velocitiesVarId[1], start, count, (float*)&(velocitiesGPU[solverGPU->getDomain()->getNumOfCells()]));
            nc_put_vara_float(fileId, velocitiesVarId[2], start, count, (float*)&(velocitiesGPU[2 * solverGPU->getDomain()->getNumOfCells()]));
        } else {
            std::cerr << "----- CLbmVisualizationNetCDF<T>::writeData() -----" << std::endl;
            std::cerr << "A datatype was specified (neither float nor double) which is not supported" << std::endl;
            std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
            std::cerr << "----------------------------------------------------" << std::endl;

            exit (EXIT_FAILURE);
        }
    }
}

template <class T>
void CLbmVisualizationNetCDF<T>::render(int iteration)
{
    if (iteration % visualizationRate == 0)
    {
        openFile(iteration);
        defineData();
        writeData();
        closeFile();
    }
}

template class CLbmVisualizationNetCDF<double>;
template class CLbmVisualizationNetCDF<float>;
