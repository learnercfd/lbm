################################################################################
# paths, directories and folders
################################################################################

CUDA_HOME			:=	$(CUDATOOLKIT_HOME)

CCLIBDIR			:=	-L$(NETCDF_DIR)/lib
CXXLIBDIR			:=	-L$(NETCDF_DIR)/lib
CUDALIBDIR			:=	

CCINCLUDES			:=	-I$(NETCDF_DIR)/include
CXXINCLUDES			:=	-I$(NETCDF_DIR)/include
CUDAINCLUDES		:=	

CCLIB				:=	-lnetcdf
CXXLIB				:=	-lnetcdf
CUDALIB				:=	

COMPUTE_CAPABILITY	:=	60

################################################################################
# compilers and linkers
################################################################################

CC					:=	
CXX					:=	
LINKER				:=	

ifeq ($(INSTRUMENT), scalasca)
CC					+=	scalasca -instrument
CXX					+=	scalasca -instrument
LINKER				+=	scalasca -instrument
endif
ifeq ($(INSTRUMENT), scorep)
CC					+=	scorep
CXX					+=	scorep
LINKER				+=	scorep
endif

CC					+=	cc
CXX					+=	CC
LINKER				+=	CC

NVCC				:=	$(CUDA_HOME)/bin/nvcc
NVCCLINKER			:=	$(CUDA_HOME)/bin/nvcc

################################################################################
# compiler arguments and flags
################################################################################

CCFLAGS				:=	-O3 \
						-qopenmp \
						-qoverride-limits \
						-xHost \
						-DMPICH_IGNORE_CXX_SEEK \
#						-parallel \
#						-g \
#						-std=c11
CXXFLAGS			:=	-O3 \
						-qopenmp \
						-qoverride-limits \
						-xHost \
						-DMPICH_IGNORE_CXX_SEEK \
#						-parallel \
#						-g \
#						-std=c11

# arch: specifies the compatibility from source code to PTX stage. Can be a
#       virtual (compute_*) or real (sm_*) compatibility.
# code: specifies the compatibility from PTX stage to binary code. Can only be
#       real (sm_*). Code has to be >= arch.
# -rdc: -rdc is short for --relocatable-device-code which generates relocatable
#       device code. This is necessary to generate multiple CUDA object files
#       which can then be linked together.
NVCCFLAGS			:=	-O3 \
						-gencode arch=compute_$(COMPUTE_CAPABILITY),code=sm_$(COMPUTE_CAPABILITY) \
#						-maxrregcount=136 \
#						--ptxas-options -v \
#						-Xcompiler "-std=c++0x" \

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS			:=	-parallel

# -dlink: Necessary linker option to link multiple CUDA object files together.
NVCCLINKERFLAGS		:=	-arch=sm_$(COMPUTE_CAPABILITY)

include common.make

